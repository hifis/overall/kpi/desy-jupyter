# Service Usage

* DESY Jupyter
* Service Description: https://helmholtz.cloud/services?serviceDetails=jupyter-desy
* Entrypoint: https://jupyter.desy.de/hub/oauth_login?next=%2Fhub%2F

## Plotting

* Plotting to be performed in the [Plotting project](https://gitlab.hzdr.de/hifis/overall/kpi/kpi-plots-ci).

## Data

* Total number of Users
* Users active in the reported month
* Cumulated CPU hors `cpu_h` for all pods that ran in the month for which the KPIs are reported

## Schedule

* monthly
