#!/usr/bin/python3

import sys
import argparse
import json
import pandas as pd
import requests as req
import urllib

parser = argparse.ArgumentParser()
parser.add_argument('-t', dest='token', default=None, type=str, nargs=1, help='jupyterhub API token with admin privileges')
parser.add_argument('-d', dest='diff', default=31, type=int, nargs='?', help='time interval between end date and begin of accumulation time in days.')
parser.add_argument('-n', dest='now', default=pd.to_datetime(pd.Timestamp.now(tz='UTC')), nargs='?', help='end of the accumulation period')
parser.add_argument('-c', dest='cpu', action='store_true', default=False, help='enable cpu metrics, currently not available (TW, Jan 24)')
parser.add_argument('--noout', dest='noout', action='store_true', default=False, help='disable result output to file, for debugging.')
args = parser.parse_args()

def get_cpu_hours(diff, now = None) -> float:
    seconds = 0

    if not now:
        query = f'sum(last_over_time(container_cpu_usage_seconds_total{{namespace="jupyterhub", pod=~"jupyter-.*"}}[{diff}d]))'
        url = f"https://query.os-prometheus01.desy.de/api/v1/query?query={urllib.parse.quote(query)}"
        response = req.get(url)
        if response.ok:
            seconds = float(response.json().get("data").get("result")[0].get("value")[1])
    else:
        realnow = pd.to_datetime(pd.Timestamp.now(tz='UTC'))
        offset = realnow - pd.to_datetime(now)

        totaldiff = offset - pd.to_timedelta(f'-{diff} days 00:00:00')
        totalquery = f'sum(last_over_time(container_cpu_usage_seconds_total{{namespace="jupyterhub", pod=~"jupyter-.*"}}[{totaldiff.days}d]))'
        url = f"https://query.os-prometheus01.desy.de/api/v1/query?query={urllib.parse.quote(totalquery)}"
        response = req.get(url)
        if response.ok:
            print(response.json())
            totalseconds = float(response.json().get("data").get("result")[0].get("value")[1])
        else:
            print(response.text)

        offsetquery = f'sum(last_over_time(container_cpu_usage_seconds_total{{namespace="jupyterhub", pod=~"jupyter-.*"}}[{offset.days}d]))'
        url = f"https://query.os-prometheus01.desy.de/api/v1/query?query={urllib.parse.quote(offsetquery)}"
        response = req.get(url)
        if response.ok:
            offsetseconds = float(response.json().get("data").get("result")[0].get("value")[1])
        else:
            print(response.text)
        seconds = totalseconds - offsetseconds

    seconds_to_hours = seconds / 60 / 60

    return seconds_to_hours

api_url = "https://jupyter.desy.de/hub/api"

if args.token is None:
    print("No API token given.")
    exit(1)
else:
    print(args.token)

r = req.get(api_url + '/users',
        headers = {
            'Authorization': f'token {args.token[0]}',
            }
        )

r.raise_for_status()
users_list = r.json()

df=pd.DataFrame.from_records(users_list)

total_users = df.shape[0]
df['timediff'] = df['last_activity'].apply(lambda x: pd.to_datetime(x) - pd.to_datetime(args.now))
active_last_month = df.loc[df['timediff'] > pd.to_timedelta(f'-{args.diff} days 00:00:00')].shape[0]

if args.cpu:
    cpu_h = get_cpu_hours(args.diff, args.now)
else:
    cpu_h = ""

print(f"Timestamp now: {args.now}")
print(f"Total number of Users: {total_users}")
print(f"Users active in the last {args.diff} days: {active_last_month}")
print(f"CPU hours, last {args.diff}d: {cpu_h}")

if not args.noout:
    with open("stats/data.csv", 'a') as datafile:
        datafile.write(f"{args.now},{total_users},{active_last_month},{cpu_h}")
else:
    print(f"{args.now},{total_users},{active_last_month},{cpu_h}")
